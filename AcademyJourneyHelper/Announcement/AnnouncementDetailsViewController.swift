import UIKit

class AnnouncementDetailsViewController: UIViewController {
	@IBOutlet weak var announcementTitleLabel: UILabel!
	@IBOutlet weak var announcementDateLabel: UILabel!
	@IBOutlet weak var announcementBodyLabel: UITextView!
	var announcement = Announcement()

	override func viewDidLoad() {
        super.viewDidLoad()
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd-MM-yyyy"
		dateFormatter.timeZone = .current
		let lastEditedDate = dateFormatter.string(from: announcement.date)
		announcementTitleLabel.text = announcement.title
		announcementBodyLabel.text = announcement.body
		announcementDateLabel.text = lastEditedDate
    }
}
