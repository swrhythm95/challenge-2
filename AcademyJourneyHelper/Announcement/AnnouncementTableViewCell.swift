import UIKit

class AnnouncementTableViewCell: UITableViewCell {
	@IBOutlet weak var announcementTitleLabel: UILabel!
	@IBOutlet weak var announcementdateLabel: UILabel!
	@IBOutlet weak var loading: UIActivityIndicatorView!
	override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
