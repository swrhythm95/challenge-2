import UIKit
import CloudKit

class AnnouncementTableViewController: UITableViewController {

	@IBOutlet weak var pullToRefreshOutlet: UIRefreshControl!
	var announcement = [Announcement]()
	let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
		navigationController?.navigationBar.prefersLargeTitles = true
        initiaCloudKitlLoader()
    }

	@IBAction func pullToRefresh(_ sender: UIRefreshControl) {
		initiaCloudKitlLoader()
	}

	func notificationObserver() {
		NotificationCenter.default.addObserver(self,
											   selector: #selector(AnnouncementTableViewController.announcementReSync),
											   name: NSNotification.Name(rawValue: "AnnouncementReSync"),
											   object: nil)
	}

	@objc func announcementReSync() {
		initiaCloudKitlLoader()
	}

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
		if announcement.count == 0 {
			return 1
		} else {
			return announcement.count
		}
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "announcementReusableCell", for: indexPath) as! AnnouncementTableViewCell
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd-MM-yyyy"
		dateFormatter.timeZone = .current

		if announcement.count != 0 {
			let fetchData = announcement[indexPath.row]
			let announcementLastEditedDate = dateFormatter.string(from: fetchData.date)
			cell.announcementTitleLabel.text = fetchData.title
			cell.announcementdateLabel.text = "\(announcementLastEditedDate)"
			cell.loading.stopAnimating()
			cell.announcementTitleLabel.isHidden = false
			cell.announcementdateLabel.isHidden = false
			cell.loading.isHidden = true
		} else {
			cell.announcementTitleLabel.text = "Loading..."
			cell.announcementdateLabel.isHidden = true
			cell.loading.isHidden = false
		}

        return cell
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let destinationVC = segue.destination as? AnnouncementDetailsViewController {
			destinationVC.announcement = announcement[tableView.indexPath(for: sender as! UITableViewCell)![1]]
		}
    }

	// MARK: Load Data from cloudKit
	func initiaCloudKitlLoader() {

		//Predictate for CloudKit
		let pred = NSPredicate(value: true)

		//Query
		let ChallengeMaster = CKQuery(recordType: "AnnounceMaster", predicate: pred)

		//Cloud Kit Operation
		let operationChallengeMaster = CKQueryOperation(query: ChallengeMaster)

		//new array od challenge
		var tempAnnouncements = [Announcement]()

		//Operation modifier
		operationChallengeMaster.desiredKeys = ["announcementTitle", "announcementBody"]
		//operationChallengeMaster.resultsLimit = 50
		operationChallengeMaster.qualityOfService = .userInteractive

		operationChallengeMaster.recordFetchedBlock = { record in
			//fill temporary challenge
			let tempAnnouncement = Announcement()
			tempAnnouncement.title = record["announcementTitle"]
			tempAnnouncement.body = record["announcementBody"]
			tempAnnouncement.date = record.modificationDate
			tempAnnouncements.append(tempAnnouncement)
		}

		operationChallengeMaster.queryCompletionBlock = { [unowned self] (cursor, error) in
			DispatchQueue.main.async {
				if error == nil {
					self.announcement = tempAnnouncements
					print("A loaded from cloud")
					print("reload Table")
					self.pullToRefreshOutlet.endRefreshing()
					self.tableView.reloadData()
				} else {
					let alert = UIAlertController(title: "Fetch Challenge failed", message: "There was a problem fetching the list of whistles; please try again: \(error!.localizedDescription)", preferredStyle: .alert)
					alert.addAction(UIAlertAction(title: "OK", style: .default))
					self.present(alert, animated: true)
				}
			}
		}

		CKContainer.default().publicCloudDatabase.add(operationChallengeMaster)
	}

}
