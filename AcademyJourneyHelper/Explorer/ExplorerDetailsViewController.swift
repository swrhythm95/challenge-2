import UIKit

extension ExplorerDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if 	similarExplorers.count == 0 {
			return 1
		} else {
			return similarExplorers.count

		}
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "similarRoleCell", for: indexPath) as! SimilarRoleCollectionViewCell
		if similarExplorers.count != 0 {
			let fetchData = similarExplorers[indexPath.row]
			cell.similarName.text = fetchData.Name
			cell.similarRole.text = fetchData.Role
			cell.similarImage.image = fetchData.ProfilePicture
			cell.loading.stopAnimating()
			cell.similarRole.isHidden = false
			cell.similarImage.isHidden = false
			cell.seniorLabel.isHidden = false
			cell.seniorLabel.text = fetchData.Seniority
			if fetchData.Seniority == "Senior"{
				cell.seniorLabel.textColor = .systemRed
			} else {
				cell.seniorLabel.textColor = .label
			}
		} else if similarExplorers == [] {
			cell.similarName.text = "No Similar Role Found"
			cell.similarRole.isHidden = true
			cell.similarImage.isHidden = true
			cell.seniorLabel.isHidden = true
			cell.loading.isHidden = true
		}
		return cell
	}
}
class ExplorerDetailsViewController: UIViewController {
	var selectedExplorer = Explorer()
	var similarExplorers = [Explorer]()
	@IBOutlet weak var explorerProfileImage: UIImageView!
	@IBOutlet weak var explorerNameLabel: UILabel!
	@IBOutlet weak var explorerRoleLabel: UILabel!
	@IBOutlet weak var iCanHelpWithLabel: UILabel!
	@IBOutlet weak var iNeedHelpWithLabel: UILabel!
	@IBOutlet weak var shiftLabel: UILabel!
	@IBOutlet weak var emailLabel: UILabel!
	@IBOutlet weak var whatsappLabel: UILabel!
	@IBOutlet weak var similarCollectionview: UICollectionView!
	@IBOutlet weak var seniorLabel: UILabel!
	@IBOutlet weak var contentScrollView: UIScrollView!
	override func viewDidLoad() {
        super.viewDidLoad()
		navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
		navigationController?.navigationBar.shadowImage = UIImage()
		navigationController?.navigationBar.isTranslucent = true
		contentScrollView.contentInsetAdjustmentBehavior = .never
		similarCollectionview.dataSource = self
		seniorLabel.text = selectedExplorer.Seniority

		if selectedExplorer.Seniority == "Senior"{
			seniorLabel.textColor = .systemRed
		} else {
			seniorLabel.textColor = .label
		}

		explorerProfileImage.image = selectedExplorer.ProfilePicture
		explorerNameLabel.text = selectedExplorer.Name
		explorerRoleLabel.text = selectedExplorer.Role
		iCanHelpWithLabel.text = "- " + (selectedExplorer.iCanHelpWith ?? "-").replacingOccurrences(of: ", ", with: "\n- ").replacingOccurrences(of: ",", with: "\n- ").replacingOccurrences(of: "; ", with: "\n- ").replacingOccurrences(of: ";", with: "\n- ")
		iNeedHelpWithLabel.text = "- " + (selectedExplorer.iWantToLearnWith ?? "-").replacingOccurrences(of: ", ", with: "\n- ").replacingOccurrences(of: ",", with: "\n- ").replacingOccurrences(of: "; ", with: "\n- ").replacingOccurrences(of: ";", with: "\n- ")
		shiftLabel.text = selectedExplorer.Shift

		if selectedExplorer.iMessage == ""{
			print(selectedExplorer.iMessage ?? "-")
			emailLabel.text = "-"
		} else {
			emailLabel.text = selectedExplorer.iMessage
		}

		if selectedExplorer.whatsapp == ""{
			whatsappLabel.text = "-"
		} else {
			whatsappLabel.text = selectedExplorer.whatsapp
		}
    }
}
