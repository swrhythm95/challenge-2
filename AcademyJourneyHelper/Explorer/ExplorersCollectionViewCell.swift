import UIKit

class ExplorersCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var ProfilePicture: UIImageView!
	@IBOutlet weak var ExplorerNAmeLabel: UILabel!
	@IBOutlet weak var ExplorerRole: UILabel!
	@IBOutlet weak var seniorLabel: UILabel!
	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
}
