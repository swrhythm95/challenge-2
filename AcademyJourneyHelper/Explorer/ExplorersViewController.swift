import UIKit
import CloudKit
import CoreData

class ExplorersViewController: UIViewController, UICollectionViewDataSource, UISearchBarDelegate {

	var explorers = [Explorer]()
	let defaults = UserDefaults.standard
	@IBOutlet weak var searchBar: UISearchBar!
	@IBOutlet weak var collectionView: UICollectionView!
	var backUpDataforSearch = [Explorer]()

	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.navigationBar.prefersLargeTitles = true
		searchBar.delegate = self
		hideKeyboard()

		notificationObserver()
		if defaults.bool(forKey: "notExplorersFirstload") {
			loadFromLocalCoreData()
		} else {
			initiaCloudKitlLoader()
			defaults.set(true, forKey: "notExplorersFirstload")
		}
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if explorers.count == 0 {
			return 1
		} else {
			return explorers.count
		}
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "explorerReuseCell", for: indexPath) as! ExplorersCollectionViewCell
		if explorers.count != 0 {
			let fetchData = explorers[indexPath.row]
			cell.ExplorerNAmeLabel.text = fetchData.Name
			cell.ExplorerRole.text = fetchData.Role
			cell.ProfilePicture.image = fetchData.ProfilePicture
			cell.seniorLabel.text = fetchData.Seniority

			if fetchData.Seniority == "Senior"{
				cell.seniorLabel.textColor = .systemRed
			} else {
				cell.seniorLabel.textColor = .label
			}

			cell.loadingIndicator.stopAnimating()
			cell.ExplorerRole.isHidden = false
			cell.ProfilePicture.isHidden = false
		} else if explorers == [] {
			cell.ExplorerNAmeLabel.text = "Loading..."
			cell.ExplorerRole.isHidden = true
			cell.ProfilePicture.isHidden = true
			cell.loadingIndicator.isHidden = false
			cell.loadingIndicator.startAnimating()
			cell.seniorLabel.isHidden = true
		}
		return cell
	}

	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let destinationVC = segue.destination as? ExplorerDetailsViewController {
			destinationVC.selectedExplorer = explorers[collectionView.indexPath(for: sender as! UICollectionViewCell)![1]]
			var similarExplorers = [Explorer]()
			for dataIndex in backUpDataforSearch {
				if dataIndex.Role == explorers[collectionView.indexPath(for: sender as! UICollectionViewCell)![1]].Role && dataIndex.ExplorerID != explorers[collectionView.indexPath(for: sender as! UICollectionViewCell)![1]].ExplorerID {
					similarExplorers.append(dataIndex)
				}
			}
			destinationVC.similarExplorers = similarExplorers
		}
	}

	// MARK: Notification Observer
	func notificationObserver() {
		NotificationCenter.default.addObserver(self,
											   selector: #selector(ExplorersViewController.explorersReSync),
											   name: NSNotification.Name(rawValue: "explorerReSync"),
											   object: nil)
	}

	@objc func explorersReSync() {
		initiaCloudKitlLoader()
	}

	// MARK: Load Data from cloudKit
	func initiaCloudKitlLoader() {
		collectionView.dataSource = self
		//CoreData
		//container set up in app delegate core data stack, this to refer to that container
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}

		//Contex from appDelegateContainer
		let managedContex = appDelegate.persistentContainer.viewContext

		//Create entity and record
		let localExplorerEntity = NSEntityDescription.entity(forEntityName: "LocalExplorer", in: managedContex)!

		//clear old core data
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "LocalExplorer")
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedContex.fetch(fetchRequest)
			for managedObject in results {
				let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
				managedContex.delete(managedObjectData)
			}
		} catch let error as NSError {
			print("Delete all data in LocalExplorer error : \(error) \(error.userInfo)")
		}

		//Predictate for cloudKit
		let pred = NSPredicate(value: true)

		//Query
		let explorersMaster = CKQuery(recordType: "explorersMaster", predicate: pred)

		//Cloud Kit Operation
		let operationChallengeMaster = CKQueryOperation(query: explorersMaster)

		//new array of challenge
		var tempExplorers = [Explorer]()

		//Operation modifier
		operationChallengeMaster.desiredKeys = ["iCanHelpWith", "iWantToLearn", "Name", "ProfilePicture", "Role", "Seniority", "Shift", "iMessage", "Whatsapp"]
		//operationChallengeMaster.resultsLimit = 50
		operationChallengeMaster.qualityOfService = .userInteractive

		operationChallengeMaster.recordFetchedBlock = { record in
			//print(record)
			let explorer = Explorer()
			explorer.ExplorerID = "\(record.recordID)"
			explorer.iCanHelpWith = record["iCanHelpWith"]
			explorer.iWantToLearnWith = record["iWantToLearn"]
			explorer.Name = record["Name"]
			explorer.Role = record["Role"]
			explorer.Seniority = record["Seniority"]
			explorer.Shift = record["Shift"]
			explorer.iMessage = record["iMessage"]
			explorer.whatsapp = record["Whatsapp"]

			//fetch cloud asset
			let asset: CKAsset? = record["ProfilePicture"] as? CKAsset
			if asset != nil {
				//parse data from cloud asset
				let photoData: NSData? = NSData(contentsOf: asset!.fileURL!)
				//generate image from fetched data
				explorer.ProfilePicture = UIImage(data: photoData! as Data)
			}
			//append to temporary journey
			tempExplorers.append(explorer)

			//convert image to binary data
			let binaryPictureFromCKAsset = explorer.ProfilePicture.pngData()

			//Add data to core date
			let tempExplorer = NSManagedObject(entity: localExplorerEntity, insertInto: managedContex)
			tempExplorer.setValue(record["iCanHelpWith"], forKeyPath: "iCanHelpWith")
			tempExplorer.setValue(record["iMessage"], forKeyPath: "iMessage")
			tempExplorer.setValue(record["iWantToLearn"], forKeyPath: "iWantToLearn")
			tempExplorer.setValue(record["Name"], forKeyPath: "name")
			tempExplorer.setValue("\(record.recordID)", forKeyPath: "recordID")
			tempExplorer.setValue(record["Role"], forKeyPath: "role")
			tempExplorer.setValue(record["Seniority"], forKeyPath: "seniority")
			tempExplorer.setValue(record["Shift"], forKeyPath: "shift")
			tempExplorer.setValue(record["Whatsapp"], forKeyPath: "whatsapp")
			tempExplorer.setValue(binaryPictureFromCKAsset, forKeyPath: "profilePicture")
			print("====\(record.recordID)")
			//Save it to core data
			do {
				try managedContex.save()
			} catch let error as NSError {
				print("______Could not save_____. \(error), \(error.userInfo)")
			}
		}

		operationChallengeMaster.queryCompletionBlock = { [unowned self] (cursor, error) in
			DispatchQueue.main.async {
				if error == nil {
					tempExplorers.sort {
						$0.Name < $1.Name
					}
					self.explorers = tempExplorers
					self.backUpDataforSearch = tempExplorers
					print("E loaded from cloud")
					print(tempExplorers)
					print("reload Collection")
					self.collectionView.reloadData()
					print("Data Loaded")
				} else {
					let alert = UIAlertController(title: "Fetch Challenge failed", message: "There was a problem fetching the explorers; please try again: \(error!.localizedDescription)", preferredStyle: .alert)
					alert.addAction(UIAlertAction(title: "OK", style: .default))
					self.present(alert, animated: true)
				}
			}
		}

		CKContainer.default().publicCloudDatabase.add(operationChallengeMaster)
	}

	// MARK: - Search Bar Function
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		if searchText != ""{
			explorers = [Explorer]()
			for dataIndex in backUpDataforSearch {
				if dataIndex.Name.uppercased().contains(searchText.uppercased()) && searchBar.selectedScopeButtonIndex == 0 {
					explorers.append(dataIndex)
				} else if dataIndex.Role.uppercased().contains(searchText.uppercased()) && searchBar.selectedScopeButtonIndex == 1 {
					explorers.append(dataIndex)
				} else if dataIndex.Shift.uppercased().contains(searchText.uppercased()) && searchBar.selectedScopeButtonIndex == 2 {
					explorers.append(dataIndex)
				}
			}
		} else {
			explorers = backUpDataforSearch
		}
		//print("search Divider:")
		//print(explorers)
		collectionView.reloadData()
	}
	// MARK: Load data from local coreData
	func loadFromLocalCoreData() {
		collectionView.dataSource = self
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
		let managedContex = appDelegate.persistentContainer.viewContext
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "LocalExplorer")

		do {
			let result = try managedContex.fetch(fetchRequest)
			//new array od challenge
			var tempExplorers = [Explorer]()

			for eachData in result as! [NSManagedObject] {
				//new array od challenge
				let tempExplorer = Explorer()

				tempExplorer.ExplorerID = (eachData.value(forKey: "recordID") ?? "") as? String
				tempExplorer.iCanHelpWith = (eachData.value(forKey: "iCanHelpWith") ?? "-") as? String
				tempExplorer.iWantToLearnWith = (eachData.value(forKey: "iWantToLearn") ?? "-") as? String
				tempExplorer.Name = (eachData.value(forKey: "name") ?? "-") as? String
				tempExplorer.Role = (eachData.value(forKey: "role") ?? "-") as? String
				tempExplorer.Seniority = (eachData.value(forKey: "seniority") ?? "-") as? String
				tempExplorer.Shift = (eachData.value(forKey: "shift") ?? "-") as? String
				tempExplorer.iMessage = (eachData.value(forKey: "iMessage") ?? "-") as? String
				tempExplorer.whatsapp = (eachData.value(forKey: "whatsapp") ?? "-") as? String
				tempExplorer.ProfilePicture = UIImage(data: (eachData.value(forKey: "profilePicture") as? Data)!)
				print("\(tempExplorer.ExplorerID ?? "")--=--\(eachData.value(forKey: "recordID") ?? "")")
				tempExplorers.append(tempExplorer)
			}

			tempExplorers.sort {
				$0.Name < $1.Name
			}
			self.explorers = tempExplorers
			self.backUpDataforSearch = tempExplorers
			print("E loaded from coreData")
			print(tempExplorers)
			print(tempExplorers[0])
			print("reload Collection")
			self.collectionView.reloadData()
			print("Data Loaded")
			print(explorers.count)
		} catch {
			print("Failed")
		}
	}
}
