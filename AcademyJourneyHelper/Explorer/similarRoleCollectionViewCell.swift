import UIKit

class SimilarRoleCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var similarImage: UIImageView!
	@IBOutlet weak var loading: UIActivityIndicatorView!
	@IBOutlet weak var similarName: UILabel!
	@IBOutlet weak var similarRole: UILabel!
	@IBOutlet weak var seniorLabel: UILabel!
}
