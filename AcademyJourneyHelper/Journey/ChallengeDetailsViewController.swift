import UIKit

class ChallengeDetailsViewController: UIViewController {

	@IBOutlet weak var challengeName: UILabel!
	@IBOutlet weak var challengePicture: UIImageView!
	@IBOutlet weak var challengeDescription: UILabel!
	@IBOutlet weak var challengeDuration: UILabel!
	@IBOutlet weak var challengeType: UILabel!
	@IBOutlet weak var challengeIsTeam: UILabel!
	@IBOutlet weak var challengeConstraint: UILabel!
	@IBOutlet weak var challengeDeliverable: UILabel!
	@IBOutlet weak var challengeLearningGoals: UILabel!
	@IBOutlet weak var personalNotes: UITextView!

	var challenge = Challenge()

	override func viewDidLoad() {
        super.viewDidLoad()
		hideKeyboard()
		challengeName.text = challenge.challengeName
		challengePicture.image = challenge.picture
		challengeDescription.text = challenge.challengeDescription
		challengeType.text = challenge.type
		challengeConstraint.text = challenge.constraint.replacingOccurrences(of: ";", with: "\n")
		challengeDeliverable.text = challenge.deliverables.replacingOccurrences(of: ";", with: "\n")
		challengeLearningGoals.text = challenge.learningGoals.replacingOccurrences(of: ";", with: "\n")

		if challenge.duration == 1 {
			challengeDuration.text = "1 week"
		} else {
			challengeDuration.text = "\(challenge.duration!) weeks"
		}

		if challenge.isTeam == 0 {
			challengeIsTeam.text = "Individual Challenge"
		} else {
			challengeIsTeam.text = "Team Challenge"
		}

		let defaults = UserDefaults.standard
		personalNotes.text = defaults.string(forKey: "\(challenge.challengeID ?? "")")
    }

	@IBAction func savePersonalNotes(_ sender: Any) {
		let defaults = UserDefaults.standard
		defaults.set(personalNotes.text, forKey: "\(challenge.challengeID ?? "")")
		let generator = UINotificationFeedbackGenerator()
		generator.notificationOccurred(.success)
		view.endEditing(true)
	}
}
