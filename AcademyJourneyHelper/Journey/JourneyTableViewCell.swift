import UIKit

class JourneyTableViewCell: UITableViewCell {

	@IBOutlet weak var challengePicture: UIImageView!
	@IBOutlet weak var challengeTitle: UILabel!
	@IBOutlet weak var challengeIsTeam: UILabel!
	@IBOutlet weak var challengeDuration: UILabel!
	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
	override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
