import UIKit
import CloudKit
import CoreData

class JourneysTableViewController: UITableViewController {

	var challenge = [Challenge]()
	let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
		navigationController?.navigationBar.prefersLargeTitles = true
		notificationObserver()

		if defaults.bool(forKey: "notJourneyFirstload") {
			loadFromLocalCoreData()
		} else {
			initiaCloudKitlLoader()
			defaults.set(true, forKey: "notJourneyFirstload")
		}
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if challenge.count == 0 {
			return 1
		} else {
			return challenge.count
		}
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "challengeCellReuseIdentifier", for: indexPath) as! JourneyTableViewCell
		if challenge.count != 0 {
			let fetchData = challenge[indexPath.row]
			cell.challengeTitle.text = fetchData.challengeName!
			if fetchData.isTeam == 0 {
				cell.challengeIsTeam.text = "Individual Challenge"
			} else {
				cell.challengeIsTeam.text = "Team Challenge"
			}
			cell.challengeDuration.text = "\(fetchData.duration!) W"
			cell.challengePicture.image = fetchData.picture
			cell.loadingIndicator.stopAnimating()
			cell.challengePicture.isHidden = false
			cell.challengeDuration.isHidden = false
			cell.challengeIsTeam.isHidden = false
		} else {
			cell.challengePicture.isHidden = true
			cell.challengeDuration.isHidden = true
			cell.challengeIsTeam.isHidden = true
		}
        return cell
    }

	// MARK: - Prepare Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let destinationVC = segue.destination as? ChallengeDetailsViewController {
			destinationVC.challenge = challenge[tableView.indexPath(for: sender as! UITableViewCell)![1]]
		}
    }

	// MARK: Notification Observer
	func notificationObserver() {
		NotificationCenter.default.addObserver(self,
											   selector: #selector(JourneysTableViewController.journeyReSync),
											   name: NSNotification.Name(rawValue: "journeyReSync"),
											   object: nil)
	}
	@objc func journeyReSync() {
		initiaCloudKitlLoader()
	}

	// MARK: Load Data from cloudKit
	func initiaCloudKitlLoader() {
		//CoreData
		//container set up in app delegate core data stack, this to refer to that container
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}

		//Contex from appDelegateContainer
		let managedContex = appDelegate.persistentContainer.viewContext

		//Create entity and record
		let localJourneyEntity = NSEntityDescription.entity(forEntityName: "LocalJourney", in: managedContex)!

		//clear old core data
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "LocalJourney")
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedContex.fetch(fetchRequest)
			for managedObject in results {
				let managedObjectData: NSManagedObject = managedObject as! NSManagedObject
				managedContex.delete(managedObjectData)
			}
		} catch let error as NSError {
			print("Delete all data in LocalJourney error : \(error) \(error.userInfo)")
		}

		//Predictate for CloudKit
		let pred = NSPredicate(value: true)

		//Query
		let challengeMaster = CKQuery(recordType: "ChallengeMaster", predicate: pred)

		//Cloud Kit Operation
		let operationChallengeMaster = CKQueryOperation(query: challengeMaster)

		//new array od challenge
		var tempChallenge = [Challenge]()

		//Operation modifier
		operationChallengeMaster.desiredKeys = ["challengeName", "batchId", "constraint", "deliverables", "description", "duration", "isTeam", "learningGoals", "type", "picture"]
		operationChallengeMaster.qualityOfService = .userInteractive

		operationChallengeMaster.recordFetchedBlock = { record in
			//fill temporary challenge
			let challenge = Challenge()
			challenge.challengeID = "\(record.recordID)"
			challenge.batchId = record["batchId"]
			challenge.challengeName = record["challengeName"]
			challenge.constraint = record["constraint"]
			challenge.deliverables = record["deliverables"]
			challenge.challengeDescription = record["description"]
			challenge.duration = record["duration"]
			challenge.isTeam = record["isTeam"]
			challenge.learningGoals = record["learningGoals"]
			challenge.type = record["type"]

			//fetch cloud asset convert to UIImage
			let asset: CKAsset? = record["picture"] as? CKAsset
			if asset != nil {
				//parse data from cloud asset
				let photoData: NSData? = NSData(contentsOf: asset!.fileURL!)
				//generate image from fetched data
				challenge.picture = UIImage(data: photoData! as Data)
			}
			//append to temp array of journey
			tempChallenge.append(challenge)

			//convert image to binary data
			let binaryPictureFromCKAsset = challenge.picture.pngData()

			//Add data to core date
			let journey = NSManagedObject(entity: localJourneyEntity, insertInto: managedContex)
			journey.setValue(record["batchId"], forKeyPath: "batchId")
			journey.setValue(record["description"], forKeyPath: "challengeDescription")
			journey.setValue(record["challengeName"], forKeyPath: "challengeName")
			journey.setValue(record["constraint"], forKeyPath: "constraint")
			journey.setValue(record["deliverables"], forKeyPath: "deliverables")
			journey.setValue(record["duration"], forKeyPath: "duration")
			journey.setValue(record["isTeam"], forKeyPath: "isTeam")
			journey.setValue(record["learningGoals"], forKeyPath: "learningGoals")
			journey.setValue(binaryPictureFromCKAsset, forKeyPath: "picture")
			journey.setValue("\(record.recordID)", forKeyPath: "recordID")
			journey.setValue(record["type"], forKeyPath: "type")

			//Save it to core data
			do {
				try managedContex.save()
			} catch let error as NSError {
				print("______Could not save_____. \(error), \(error.userInfo)")
			}
		}

		operationChallengeMaster.queryCompletionBlock = { [unowned self] (cursor, error) in
			DispatchQueue.main.async {
				if error == nil {
					self.challenge = tempChallenge
					print("C loaded from cloud")
					print("reload Table")
					self.tableView.reloadData()
				} else {
					let alert = UIAlertController(title: "Fetch Challenge failed", message: "There was a problem fetching the Challenge data; please try again: \(error!.localizedDescription)", preferredStyle: .alert)
					alert.addAction(UIAlertAction(title: "OK", style: .default))
					self.present(alert, animated: true)
				}
			}
		}
		CKContainer.default().publicCloudDatabase.add(operationChallengeMaster)
	}

	// MARK: Load data from local coreData
	func loadFromLocalCoreData() {
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
		let managedContex = appDelegate.persistentContainer.viewContext
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "LocalJourney")

		do {
			let result = try managedContex.fetch(fetchRequest)
			//new array od challenge
			var tempChallenge = [Challenge]()

			for eachData in result as! [NSManagedObject] {
				//new array od challenge
				let challenge = Challenge()
				challenge.challengeID = eachData.value(forKey: "recordID")! as? String
				challenge.batchId = (eachData.value(forKey: "batchId") ?? 0) as? Int
				challenge.challengeName = (eachData.value(forKey: "challengeName") ?? "-") as? String
				challenge.constraint = (eachData.value(forKey: "constraint") ?? "-") as? String
				challenge.deliverables = (eachData.value(forKey: "deliverables") ?? "-") as? String
				challenge.challengeDescription = (eachData.value(forKey: "challengeDescription") ?? "-") as? String
				challenge.duration = (eachData.value(forKey: "duration") ?? 0) as? Int
				challenge.isTeam = (eachData.value(forKey: "isTeam") ?? 0) as? Int
				challenge.learningGoals = (eachData.value(forKey: "learningGoals") ?? "-") as? String
				challenge.type = (eachData.value(forKey: "type") ?? "-") as? String
				challenge.picture = UIImage(data: (eachData.value(forKey: "picture") as? Data)!)
				tempChallenge.append(challenge)
			}

			self.challenge = tempChallenge
			print("C loaded from coreData")
			print("reload Table")
			self.tableView.reloadData()
		} catch {
			print("Failed")
		}
	}
}
