import UIKit

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
		navigationController?.navigationBar.prefersLargeTitles = true
    }

	@IBAction func reSyncButton(_ sender: Any) {
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: "journeyReSync"), object: nil)
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: "explorerReSync"), object: nil)
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AnnouncementReSync"), object: nil)
		let generator = UINotificationFeedbackGenerator()
		generator.notificationOccurred(.success)
	}
}
