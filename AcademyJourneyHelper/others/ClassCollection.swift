import Foundation
import UIKit
import CloudKit

class Challenge: NSObject {
	var challengeID: String!
	var batchId: Int!
	var challengeName: String!
	var constraint: String!
	var deliverables: String!
	var challengeDescription: String!
	var duration: Int!
	var isTeam: Int!
	var learningGoals: String!
	var picture: UIImage!
	var type: String!
}

class Explorer: NSObject {
	var ExplorerID: String!
	var iCanHelpWith: String!
	var iWantToLearnWith: String!
	var Name: String!
	var ProfilePicture: UIImage!
	var Role: String!
	var Seniority: String!
	var Shift: String!
	var iMessage: String!
	var whatsapp: String!
}

class Announcement: NSObject {
	var title: String!
	var date: Date!
	var body: String!
}
