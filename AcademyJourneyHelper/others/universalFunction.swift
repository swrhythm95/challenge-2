import Foundation
import UIKit

extension UIViewController {
	func hideKeyboard() {
		let Tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
		Tap.cancelsTouchesInView = false
		view.addGestureRecognizer(Tap)
	}
	@objc func dismissKeyboard() {
		view.endEditing(true)
	}
}
