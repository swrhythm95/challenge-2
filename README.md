#  Challenge 2 - Steven wijaya
This app is the second challenge in Apple Developer Academy. The Covid-19 pandemic thet make lots of people need to stay at home for self quarantine and working remotely. From that reason this app is made so The Academy can give the challenge to the explorers in the journey tab, the explorers can view each other skillset and info in the explorers tab, The Academy can boardcast information in the announcement tab.

## Feature
1. On app first launch will fetch data from cloudKit. after finished fetch all data the data will be saved in coreData so the socond and on app launch will be faster and load the table and collection view.
2. In Journey Details view there's feature to taking notes for that challenge in the botton part. upon clicking save the user will get haptic feedback indicating the saving progress is done. this feature using UserDefault.
3. On the explorer view the user can search either name, role or shift by clicking the search scope and type in the search bar.
4. There's announcement tab that will always fecth from cloudKit on lauch so the data will always updated. 
5. To refresh data there's "ReSync" button on setings page to reload feature number 1 and pull to refresh on announcement page to fetch new data.

## Need to know
1. user need to login to iCloud before opening the app so cloudKit process can be done.

## Storyboard
```mermaid
graph LR;
  A[TabBar Controller]-->B[Navigation Controller];
  A-->C[Navigation Controller];
  A-->D[Navigation Controller];
  B-->E[Journeys Table View];
  E-->F[Journey Details View];
  C-->G[Explorers Collection View];
  G-->H[Explorer Details View];
  D-->I[Announcements Table View];
  I-->J[Announcement Details View];
  A-->K[Navigation Controller];
  K-->L[Settings]
```

## Things used in this project so far
- TableView
- CollectionView
- ScrollView
- TabBar
- NavBar
- Segue
- SearchBar with scope
- Autolayout
- UsedDefault
- CoreData
- CloudKit
- Pull to refresh
- Label
- TextView
- ImageView
- Button
- System color
- Text Style
###### and other stuff for enhancing the app

## Created by - [Steven Wijaya](https://gitlab.com/swrhythm95)
- [Linkedin](https://www.lingkedin.com/in/stevenwijaya95)
- [Email](mailto:stevenwijaya2195@gmail.com?subject=Challenge%202@20-@20GitLab)

